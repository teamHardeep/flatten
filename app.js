var arrayToFlatten = [1,[2,[10,10,20]],[23,6]];

var flattenedArray = [];

var recursiveFlattening = function( arr ) {
	arr.forEach(function( val ) {
		if( val ) {
		if( Object.prototype.toString.call( val ) === '[object Array]' ) {
	    		recursiveFlattening( val );
			} else {
				flattenedArray.push( val );
			}
		}
	});
};

recursiveFlattening( arrayToFlatten );

console.log( JSON.stringify( flattenedArray ) )